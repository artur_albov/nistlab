package by.albov;

import java.util.Arrays;

/**
 * Created by artur on 20.3.16.
 */
public class DiscreteFourierTransform {

    public static float[] fftF(float[] x) {

        int N = x.length/2;

        float[] b = new float[x.length];


        for(int i = 0 ; i < N/2 ; i++) {
            b[2*i] = x[2*i] + x[2*(i+N/2)];
            b[2*i+1] = x[2*i+1] + x[2*(i+N/2)+1];

            b[2*(i+N/2)] = x[2*i] - x[2*(i+N/2)];
            b[2*(i+N/2)+1] = x[2*i+1] - x[2*(i+N/2)+1];
        }

        int K = N/4;

        while (K != 0) {
            for(int i = 0 ; i < b.length; i++) {
                x[i] = b[i];
            }
            for (int i = 0; i < N / 2; i++) {
                float w_re;
                float w_im;
                if ((i/(K)) % (N/(K)) == 0) {
                    w_re = 1;
                    w_im = 0;
                } else {
                    float arg = (float) (-2 * Math.PI * (i/(K)) / (N/(K)));
                    w_re = (float)Math.cos(arg);
                    w_im = (float)Math.sin(arg);
                }

                float real = w_re * x[2*((i/(K))*K+i+K)] - w_im * x[2*((i/(K))*K+i+K)+1];
                float imag = w_re * x[2*((i/(K))*K+i+K)+1] + w_im * x[2*((i/(K))*K+i+K)];

                b[2*i] = x[2*((i/(K))*K+i)] + real;
                b[2*i+1] = x[2*((i/(K))*K+i)+1] + imag;
                b[2*(i + N/2)] = x[2*((i/(K))*K+i)] - real;
                b[2*(i + N/2) + 1] = x[2*((i/(K))*K+i)+1] - imag;
            }
            K = K/2;
        }

        return b;

    }

    private static float[] calculateModules(float[] complexSeq) {
        float[] result = new float[complexSeq.length/4];
        for(int i = 0; i < complexSeq.length/2 ; i+=2) {
            result[i/2] = (float)Math.hypot(complexSeq[i],complexSeq[i+1]);
        }
        return result;
    }

    public static float[] modules(byte[] seq) {
        float[] x = new float[seq.length*2];
        for (int i = 0 ; i < x.length ; i+=2) {
            x[i] = seq[i/2];
            x[i+1] = 0;
        }
        return calculateModules(fftF(x));
    }


}
