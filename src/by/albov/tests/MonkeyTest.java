package by.albov.tests;

import by.albov.TestUtils;

/**
 * Created by artur on 20.3.16.
 */
public class MonkeyTest {


        /*
     * ========================================================================
     * This is the Diehard OPSO test, rewritten from the description
     * in tests.txt on George Marsaglia's diehard site.
     *
     *         OPSO means Overlapping-Pairs-Sparse-Occupancy         ::
     * The OPSO test considers 2-letter words from an alphabet of    ::
     * 2^10 (10-bit) letters.  Each letter is determined by a specified ten   ::
     * bits from a 32-bit integer in the sequence to be tested. OPSO ::
     * generates  2^21 (overlapping) 2-letter words  (from 2^21+1    ::
     * "keystrokes")  and counts the number of missing words---that  ::
     * is 2-letter words which do not appear in the entire sequence. ::
     * That count should be very close to normally distributed with  ::
     * mean 141,909, sigma 290. Thus (missingwrds-141909)/290 should ::
     * be a standard normal variable. The OPSO test takes 32 bits at ::
     * a time from the test file and uses a designated set of ten    ::
     * consecutive bits. It then restarts the file for the next de-  ::
     * signated 10 bits, and so on.                                  ::
     *
     * Note: Overlapping samples must be used to get the right sigma.
     * The tests BITSTREAM, OPSO, OQSO and DNA are all closely related.
     *
     * This test is now CORRECTED on the basis of a private communication
     * from Paul Leopardi (MCQMC-2008 presentation) and Noonan and Zeilberger
     * (1999), Rivals and Rahmann (2003), and Rahmann and Rivals (2003).
     * The "exact" target statistic (to many places) is:
     * \mu  = 141909.3299550069,  \sigma = 290.4622634038
     *========================================================================
     */

    public boolean process(String sequence) {
        byte[][] wordsMatchings = new byte[1024][1024];

        double mean = 141909.3299550069;
        double sigma = 290.4622634038;

        int t = (int) (Math.pow(2,21) + 1);

        for(int i = 0 ; i < 20*Math.pow(2,21) ; i+=20) {
            int l = Integer.valueOf(sequence.substring(i,i+10),2);
            int r = Integer.valueOf(sequence.substring(i + 10, i + 20),2);
            wordsMatchings[l][r] = 1;
        }

        int missingWordsNumber = 0;
        for(int i = 0 ; i < 1024 ; i++) {
            for(int j = 0 ; j < 1024; j++) {
                if(wordsMatchings[i][j] == 0) {
                    missingWordsNumber++;
                }
            }
        }

        // Should be normally distributed value
        double x = (missingWordsNumber - mean) / sigma;



        System.out.println("Missing word number: " + missingWordsNumber);
        System.out.println("p-Value: " + (1-TestUtils.calculatePhi(x)));
        return true;
    }

}
