package by.albov.tests;

/**
 * Created by artur on 20.3.16.
 */
public interface Test {
    boolean process(String sequence);
}
