package by.albov.tests;

import by.albov.Complex;
import by.albov.DiscreteFourierTransform;
import by.albov.TestUtils;

/**
 * Created by artur on 20.3.16.
 */
public class SpectralTest {

    private int n;
    private double peakHeight;
    private double expectedPeaksNumber;

    public SpectralTest() {
    }

    private double[] calculateModules(Complex[] complexSeq) {
        double[] result = new double[n/2];
        for(int i = 0; i < result.length ; i++) {
            result[i] = complexSeq[i].abs();
        }
        return result;
    }

    private double getPeakHeight() {
        return Math.sqrt(Math.log(1 / 0.05)*n);
    }

    private double getExpectedPeaksNumber() {
        return 0.95*n/2;
    }

    private double calculateRealPeaksNumber(float[] modules) {
        double count = 0;
        for (float module : modules) {
            if(module < peakHeight) {
                count++;
            }
        }
        return count;
    }

    private double calculateD(double realPeaksNumber) {
        return (realPeaksNumber - expectedPeaksNumber)/Math.sqrt(n*0.95*0.05/4);
    }

    private double calculatePValue(double d) {
        return 2*(1-TestUtils.calculatePhi(Math.abs(d)));
    }


    public boolean process(byte[] sequence) {
        this.n = sequence.length;
        this.peakHeight = getPeakHeight();
        this.expectedPeaksNumber = getExpectedPeaksNumber();

//        Complex[] res = DiscreteFourierTransform.fft(TestUtils.convertZeroOnes(sequence));
        float[] modules = DiscreteFourierTransform.modules(TestUtils.convertZeroOnes(sequence));
//        calculateModules(res);
        double realPeaksNumber = calculateRealPeaksNumber(modules);
        double d = calculateD(realPeaksNumber);
        double pValue = calculatePValue(d);
        System.out.println("p-Value: " + pValue);
        return pValue >= 0.01;
    }


}
