package by.albov.tests;

import by.albov.TestUtils;

/**
 * Created by artur on 20.3.16.
 */
public class NonOverlappingTemplateMatchingTest implements Test {
    private int n;

    private String B = "110";
    private int M;
    private int N = 90;
    private int m = 3;

    private double dispersion;
    private double expectedValue;


    private NonOverlappingTemplateMatchingTest() {
    }

    public NonOverlappingTemplateMatchingTest(int n, int N) {
        this.n = n;
        this.N = N;
        this.M = n/N;
        this.expectedValue = getExpectedValue();
        this.dispersion = getDispersion();
    }



    @Override
    public boolean process(String sequence) {
        String[] blocks = getBlocks(sequence);
        int[] matchingsNumbers = calculateMatchings(blocks);
        double xiSquare = calculateXiSquare(matchingsNumbers);

        double pValue = TestUtils.incompletegammac(N/2, xiSquare/2);
        System.out.println("p-Value: "+pValue);
        return pValue >= 0.01;
    }

    private double calculateXiSquare(int[] matchingsNumbers) {
        double res = 0;
        for (int i = 0 ; i < N ; i++) {
            res += Math.pow(matchingsNumbers[i]-expectedValue,2)/dispersion;
        }
        return res;
    }

    private int[] calculateMatchings(String[] blocks) {
        int[] result = new int[N];
        for(int i = 0 ; i < N ; i++) {
            result[i] = processBlock(blocks[i]);
        }
        return result;
    }

    private int processBlock(String block) {
        int count = 0;

        int i = 0;
        while(i < block.length() - m) {
            if(block.substring(i,i+m).equals(B)) {
                count++;
                i += m;
            } else {
                i++;
            }
        }

        return count;

    }

    private String[] getBlocks(String sequenceStr) {
        String[] blocks = new String[N];
        for(int i = 0 ; i < N; i++) {
            blocks[i] = sequenceStr.substring(M*i,M*(i+1) >= n ? n :  M*(i+1));
        }
        return blocks;
    }

    public double getExpectedValue() {
        return (M-m+1)/Math.pow(2,m);
    }

    public double getDispersion() {
        return M*( (1/Math.pow(2,m)) - ((2*m-1)/Math.pow(2, 2*m)) );
    }
}
