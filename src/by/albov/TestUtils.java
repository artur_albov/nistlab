package by.albov;

import org.apache.commons.math3.special.Erf;

/**
 * Created by artur on 20.3.16.
 */
public class TestUtils {


    // Преобразуем массив 0 и 1 в -1 и 1 для преобразования Фурье
    public static byte[] convertZeroOnes(byte[] sequence) {
        byte[] result = new byte[sequence.length];
        for (int i = 0; i < result.length; i++) {
            result[i] = (byte) (2 * sequence[i] - 1);
        }
        return result;
    }

    public static int binlog(int bits) {
        int log = 0;
        if ((bits & 0xffff0000) != 0) {
            bits >>>= 16;
            log = 16;
        }
        if (bits >= 256) {
            bits >>>= 8;
            log += 8;
        }
        if (bits >= 16) {
            bits >>>= 4;
            log += 4;
        }
        if (bits >= 4) {
            bits >>>= 2;
            log += 2;
        }
        return log + (bits >>> 1);
    }

    // Используя + для конкантенации получаем new StringBuilder.append("...") на каждой итерации,
    // что приводит к созданию большого кол-ва объектов и дублированию строк => очень долгая работа.
    // Поэтому используем изначально StringBuilder и получаем оптимальное преобразование
    public static String convertToBits(byte[] sequence) {
        StringBuilder builder = new StringBuilder("");
        for (byte b : sequence) {
            builder.append(String.format("%8s", Integer.toBinaryString(b & 0xFF)).replace(' ', '0'));
        }
        return builder.toString();
    }

    public static byte[] convertStringToBitsArray(String bitString) {
        byte[] result = new byte[(int) Math.pow(2, binlog(bitString.length()))];
        for (int i = 0; i < result.length; i++) {
            result[i] = Byte.valueOf(String.valueOf(bitString.charAt(i)));
        }
        return result;
    }


    public static double incompletegamma(double a,
                                         double x) {
        double result = 0;
        double igammaepsilon = 0;
        double ans = 0;
        double ax = 0;
        double c = 0;
        double r = 0;
        double tmp = 0;

        igammaepsilon = 0.000000000000001;
        if ((double) (x) <= (double) (0) | (double) (a) <= (double) (0)) {
            result = 0;
            return result;
        }
        if ((double) (x) > (double) (1) & (double) (x) > (double) (a)) {
            result = 1 - incompletegammac(a, x);
            return result;
        }
        ax = a * Math.log(x) - x - lngamma(a, tmp);
        if ((double) (ax) < (double) (-709.78271289338399)) {
            result = 0;
            return result;
        }
        ax = Math.exp(ax);
        r = a;
        c = 1;
        ans = 1;
        do {
            r = r + 1;
            c = c * x / r;
            ans = ans + c;
        }
        while ((double) (c / ans) > (double) (igammaepsilon));
        result = ans * ax / a;
        return result;
    }

    public static double incompletegammac(double a, double x) {
        double result = 0;
        double igammaepsilon = 0;
        double igammabignumber = 0;
        double igammabignumberinv = 0;
        double ans = 0;
        double ax = 0;
        double c = 0;
        double yc = 0;
        double r = 0;
        double t = 0;
        double y = 0;
        double z = 0;
        double pk = 0;
        double pkm1 = 0;
        double pkm2 = 0;
        double qk = 0;
        double qkm1 = 0;
        double qkm2 = 0;
        double tmp = 0;

        igammaepsilon = 0.000000000000001;
        igammabignumber = 4503599627370496.0;
        igammabignumberinv = 2.22044604925031308085 * 0.0000000000000001;
        if ((double) (x) <= (double) (0) | (double) (a) <= (double) (0)) {
            result = 1;
            return result;
        }
        if ((double) (x) < (double) (1) | (double) (x) < (double) (a)) {
            result = 1 - incompletegamma(a, x);
            return result;
        }
        ax = a * Math.log(x) - x - lngamma(a, tmp);
        if ((double) (ax) < (double) (-709.78271289338399)) {
            result = 0;
            return result;
        }
        ax = Math.exp(ax);
        y = 1 - a;
        z = x + y + 1;
        c = 0;
        pkm2 = 1;
        qkm2 = x;
        pkm1 = x + 1;
        qkm1 = z * x;
        ans = pkm1 / qkm1;
        do {
            c = c + 1;
            y = y + 1;
            z = z + 2;
            yc = y * c;
            pk = pkm1 * z - pkm2 * yc;
            qk = qkm1 * z - qkm2 * yc;
            if ((double) (qk) != (double) (0)) {
                r = pk / qk;
                t = Math.abs((ans - r) / r);
                ans = r;
            } else {
                t = 1;
            }
            pkm2 = pkm1;
            pkm1 = pk;
            qkm2 = qkm1;
            qkm1 = qk;
            if ((double) (Math.abs(pk)) > (double) (igammabignumber)) {
                pkm2 = pkm2 * igammabignumberinv;
                pkm1 = pkm1 * igammabignumberinv;
                qkm2 = qkm2 * igammabignumberinv;
                qkm1 = qkm1 * igammabignumberinv;
            }
        }
        while ((double) (t) > (double) (igammaepsilon));
        result = ans * ax;
        return result;
    }

    public static double lngamma(double x,
                                 Double sgngam) {
        double result = 0;
        double a = 0;
        double b = 0;
        double c = 0;
        double p = 0;
        double q = 0;
        double u = 0;
        double w = 0;
        double z = 0;
        int i = 0;
        double logpi = 0;
        double ls2pi = 0;
        double tmp = 0;

        sgngam = (double) 0;

        sgngam = (double) 1;
        logpi = 1.14472988584940017414;
        ls2pi = 0.91893853320467274178;
        if ((double) (x) < (double) (-34.0)) {
            q = -x;
            w = lngamma(q, tmp);
            p = (int) Math.floor(q);
            i = (int) Math.round(p);
            if (i % 2 == 0) {
                sgngam = (double) -1;
            } else {
                sgngam = (double) 1;
            }
            z = q - p;
            if ((double) (z) > (double) (0.5)) {
                p = p + 1;
                z = p - q;
            }
            z = q * Math.sin(Math.PI * z);
            result = logpi - Math.log(z) - w;
            return result;
        }
        if ((double) (x) < (double) (13)) {
            z = 1;
            p = 0;
            u = x;
            while ((double) (u) >= (double) (3)) {
                p = p - 1;
                u = x + p;
                z = z * u;
            }
            while ((double) (u) < (double) (2)) {
                z = z / u;
                p = p + 1;
                u = x + p;
            }
            if ((double) (z) < (double) (0)) {
                sgngam = (double) -1;
                z = -z;
            } else {
                sgngam = (double) 1;
            }
            if ((double) (u) == (double) (2)) {
                result = Math.log(z);
                return result;
            }
            p = p - 2;
            x = x + p;
            b = -1378.25152569120859100;
            b = -38801.6315134637840924 + x * b;
            b = -331612.992738871184744 + x * b;
            b = -1162370.97492762307383 + x * b;
            b = -1721737.00820839662146 + x * b;
            b = -853555.664245765465627 + x * b;
            c = 1;
            c = -351.815701436523470549 + x * c;
            c = -17064.2106651881159223 + x * c;
            c = -220528.590553854454839 + x * c;
            c = -1139334.44367982507207 + x * c;
            c = -2532523.07177582951285 + x * c;
            c = -2018891.41433532773231 + x * c;
            p = x * b / c;
            result = Math.log(z) + p;
            return result;
        }
        q = (x - 0.5) * Math.log(x) - x + ls2pi;
        if ((double) (x) > (double) (100000000)) {
            result = q;
            return result;
        }
        p = 1 / (x * x);
        if ((double) (x) >= (double) (1000.0)) {
            q = q + ((7.9365079365079365079365 * 0.0001 * p - 2.7777777777777777777778 * 0.001) * p + 0.0833333333333333333333) / x;
        } else {
            a = 8.11614167470508450300 * 0.0001;
            a = -(5.95061904284301438324 * 0.0001) + p * a;
            a = 7.93650340457716943945 * 0.0001 + p * a;
            a = -(2.77777777730099687205 * 0.001) + p * a;
            a = 8.33333333333331927722 * 0.01 + p * a;
            q = q + a / x;
        }
        result = q;
        return result;
    }

    public static double calculatePhi(double x) {
        // constants
        double a1 = 0.254829592;
        double a2 = -0.284496736;
        double a3 = 1.421413741;
        double a4 = -1.453152027;
        double a5 = 1.061405429;
        double p = 0.3275911;

        // Save the sign of x
        int sign = 1;
        if (x < 0)
            sign = -1;
        x = Math.abs(x) / Math.sqrt(2.0);

        // A&S formula 7.1.26
        double t = 1.0 / (1.0 + p * x);
        double y = 1.0 - (((((a5 * t + a4) * t) + a3) * t + a2) * t + a1) * t * Math.exp(-x * x);

        return 0.5 * (1.0 + sign * y);
    }

    public static double phi(double x) {
        double tmp=x/Math.sqrt(2.);
        tmp=1+ Erf.erf(tmp);
        return tmp/2;
    }

    public static double getProbAB(int n) {
        return 1.000002861032044196*Math.pow(0.99999904632477409731349945987,n);
    }

    public static double getProbAA(int n) {
        return 1.000000951817114053298*Math.pow(0.99999904725519072388710142577,n);
    }

    public static double getMonkeyMean(double n) {
        return Math.pow(2,20)*Math.exp(-n/Math.pow(2,20));
    }

    public static double getMonkeyVariance(double n) {
        return 0d;
    }


}
