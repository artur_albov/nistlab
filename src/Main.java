import by.albov.TestUtils;
import by.albov.tests.MonkeyTest;
import by.albov.tests.NonOverlappingTemplateMatchingTest;
import by.albov.tests.SpectralTest;

import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {

    public static void main(String[] args) throws Exception {

//
        byte[] seq = Files.readAllBytes(Paths.get("seq1.bin"));
        String sequenceStr = TestUtils.convertToBits(seq);


        byte[] key = Files.readAllBytes(Paths.get("seq7.bin"));

//        SpectralTest spectralTest = new SpectralTest();
//        NonOverlappingTemplateMatchingTest matchingTest = new NonOverlappingTemplateMatchingTest(sequenceStr.length(), 90);
        MonkeyTest monkeyTest = new MonkeyTest();

//        System.out.println("------ SPECTRAL TEST ------");
//        System.out.println(spectralTest.process(TestUtils.convertStringToBitsArray(sequenceStr)) ? "random" : "non random");
//        System.out.println("------ NON-OVERLAPPING TEMPLATE MATCHING TEST ------");
//        System.out.println(matchingTest.process(sequenceStr) ? "random" : "non random");
        System.out.println("------ MONKEY TEST OPSO ------");
        System.out.println(monkeyTest.process(sequenceStr) ? "random" : "non random");

//        double mean = (Math.pow(2,20)-Math.pow(2, 10))*TestUtils.getProbAB((int)Math.pow(2,21)) + Math.pow(2,10) *TestUtils.getProbAA((int) Math.pow(2, 21));
//        double n = Math.pow(2,21);
//        System.out.println(mean);
        System.out.println(  );



    }
}
